<div class="content" >
      <center > <h1  style="color: SteelBlue" >BIENVENIDOS A FEDEX.COM</h1>   </center >
          <img  src="<?php echo base_url(); ?>/assets/plantilla/img/f4.png"   width="100%" height="300px"  alt="...">
          <br>
           <br>
        <div class="row" >
  <div class="col-xs-6 col-md-12">
    <h1 class="text-center" style="color: SteelBlue" >Más de FedEx</h1>
<p class="text-center">
  Para tus necesidades de importación, exportación o locales, paquetes livianos o pesados, envíos con carácter de urgente o sin tanta prisa,
  FedEx te ofrece soluciones y un servicio en los cuales puedes confiar.
</p>
  </div>
  </div>
<div class="row">
<div class="col-sm-6 col-md-4">
  <div class="thumbnail">
    <img src="<?php echo base_url(); ?>/assets/plantilla/img/f2.png" width="100%" height="200px"  alt="...">
    <div class="caption">
      <br>
      <h3>¿Eres nuevo en FedEx? </h3>
      <p class="text-justify">
    Nuestro nuevo Centro de Atención al Cliente te guía en todos los pasos necesarios para hacer un envío con FedEx.
      </p>
    </div>
  </div>
</div>
<div class="col-sm-6 col-md-4">
  <div class="thumbnail">
    <img src="<?php echo base_url(); ?>/assets/plantilla/img/f1.png" width="100%" height="200px"  alt="...">
    <div class="caption">
      <br>
      <h3>Estamos realizando entregas.</h3>
      <p class="text-justify">
        En FedEx, somos más de 475,000 empleados en todo el mundo trabajando para ti.
        Conectamos Posibilidades.
    </p>
    </div>
  </div>
</div>
<div class="col-sm-6 col-md-4">
  <div class="thumbnail">
    <img src="<?php echo base_url(); ?>/assets/plantilla/img/f5.png" width="100%" height="200px"  alt="..." >
    <div class="caption">
      <br>
      <h3>Servicios de envío</h3>
      <p class="text-justify">
        Elije FedEx para entregar tus envíos importantes y urgentes desde y hacia más de 220 países y territorios en todo el mundo.
      </p>
    </div>
  </div>
</div>
<br>
</div>
<div class="row">
  <div class="border-top text-center"><hr size="3px" color: "white"> </div>
</div>
  <center>
  <div class="row">
    <div class="col-md-2">
      <h4 style="color: SteelBlue"><strong>Desarrollado por:</strong></h4>
      <ul>
        <li style="color: black">Porras Wilson</li>
        <li style="color: black">Ayala Martha</li>
      </ul>
    </div>
    <div class="col-md-8">
      <h4 style="color: SteelBlue">Redes sociales</h4>
      <hr>
      <a href="https://www.facebook.com/wilson.porrasatiaja"><img src=" <?php echo base_url(); ?>/assets/plantilla/img/facebook.png " alt="facebook"></a>
      <a href="https://www.instagram.com/porrasatiaja/"><img src=" <?php echo base_url(); ?>/assets/plantilla/img//instagram.png " alt="instagram"></a>
      <a href="https://www.whatsapp.com/download"><img src=" <?php echo base_url(); ?>/assets/plantilla/img/whatsapp.png " alt="whatsapp"></a>
      <a href="https://www.tiktok.com/foryou"><img src=" <?php echo base_url(); ?>/assets/plantilla/img/tik-tok.png " alt="tik-tok"></a>
      <a href="https://www.youtube.com/channel/UCzFMwuuqDT06ud7JGCs0i4A"><img src=" <?php echo base_url(); ?>/assets/plantilla/img//youtube.png " alt="youtube"></a>
    </div>
    <div class="col-md-2">
      <h4 style="color: SteelBlue">Nosotros:</h4>
      <p style="color: black">Quienes somos?</p>
      <p style="color: black">Nuestro Objetivo</p>
    </div>
  </div>
  </center>
<div class="d-sm-flex justify-content-center justify-content-sm-between">
  <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Desarrollo Web</span>
  <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"><a href="https://www.bootstrapdash.com/bootstrap-admin-template/" target="_blank">
© FedEx 1995-2023
</a></span>
</div>
</div>
